import React from 'react'
import { Alert } from 'antd';

function NotFound() {
    return (
        <Alert
        message='Uyarı'
        className="warning-alert"
        description="Ulaşmaya çalıştığınız sayfa bulunamamıştır, lütfen kontrol ediniz !"
        type="warning"
        showIcon
      />
    )
}

export default NotFound;
