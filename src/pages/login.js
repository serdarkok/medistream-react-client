import React, { useState } from 'react';
import { Row, Col, Form, Input, Button, Select, Checkbox } from 'antd';
import { CalendarOutlined, FundProjectionScreenOutlined, TeamOutlined } from '@ant-design/icons';

import 'antd/dist/antd.css';
import '../assets/scss/main.scss';
import logo from '../assets/images/sandoz-logo.png';

const titleList = [
    {id: 1, name: 'Prof'},
    {id: 2, name: 'Doç'},
    {id: 3, name: 'Öğr. Gör.'},
    {id: 4, name: 'Uzm'},
    {id: 5, name: 'Asistan'},
    {id: 6, name: 'Aile Hekimi'},
    {id: 7, name: 'Eczacı'},
    {id: 8, name: 'Diğer'},
]

const branchList = [
    {id: 1, name: 'Kardiyoloji' },
    {id: 2, name: 'Göğüs Hastalıkları' },
    {id: 3, name: 'Gastroenteroloji Uzmanı' },
    {id: 4, name: 'Aile Hekimi' },
    {id: 5, name: 'Diğer' },
]

const titleSelectOptions = () => (
    <Select>
        {titleList.map(item => {
            return <Select.Option value={item.id} key={item.id}> {item.name} </Select.Option>
        })}
    </Select>
)

const branchSelectOptions = () => {
    return (
        <Select>
            {branchList.map(item => <Select.Option value={item.id} key={item.id}> {item.name} </Select.Option>)}
        </Select>
    )
}

const LoginPage = () => {
    const [tcNo, settcNo] = useState('');

    const handleInput = (value) => {
        const re = /^[0-9\b]+$/;
        if (value === '' || re.test(value)) { 
            settcNo(value);
        }
    }
    return (
        <>
            <div className={'bg-global'}></div>
                <Row style={{padding: '8px'}}>
                    <Col xs={{ span: 24 }} lg={{ span: 8, offset: 4 }}>
                        <div className="event-information">
                            <div className="company-logo">
                                <img src={logo} alt={logo} /> 'katkılarıyla...
                        </div>
                            <div><CalendarOutlined className="icon-inline" /> Etkinlik Tarihi
                        <span>12 Eylül 2020 Saat 16:00</span>
                            </div>
                            <div><FundProjectionScreenOutlined className="icon-inline" /> Etkinlik Konusu
                        <span>İBUPROFEN'İN KAS-İSKELET AĞRISI TEDAVİSİNDE ETKİNLİĞİ VE GÜVENLİLİĞİ</span>
                            </div>
                            <div><TeamOutlined className="icon-inline" /> Konuşmacılar
                        <span>
                        Prof. Dr. Birol Özer <br />
                        Prof. Dr. Tarkan Karakan <br />
                        Prof. Dr. Serhat Bor
                        </span>
                            </div>
                        </div>
                    </Col>
                    <Col xs={{ span: 24 }} lg={{ span: 8 }}>
                        <div className="login-wrapper">
                            <Form layout="vertical" size="large">
                                <Form.Item label="Unvan">
                                    { titleSelectOptions() }
                                </Form.Item>
                                <Form.Item name="Name" label="İsim Soyisim">
                                    <Input></Input>
                                </Form.Item>
                                <Form.Item label="TC Kimlik No">
                                    <Input maxLength={11} value={tcNo} onChange={(e) => handleInput(e.target.value)}></Input>
                                </Form.Item>
                                <Form.Item  name="branch" label="Branş" rules={[{ required: true }]}>
                                    { branchSelectOptions() }
                                </Form.Item>
                                <Form.Item
                                    noStyle
                                    shouldUpdate={(prevValues, currentValues) => prevValues.branch !== currentValues.branch}
                                >
                                    {({ getFieldValue }) => {
                                        return getFieldValue('branch') === branchList[branchList.length - 1].id ? (
                                            <Form.Item
                                                name="other"
                                                label="Lütfen Belirtiniz"
                                                rules={[{ required: true }]}
                                            >
                                                <Input />
                                            </Form.Item>
                                        ) : null;
                                    }}
                                </Form.Item>
                                <Form.Item name="email" label="E-Posta">
                                    <Input></Input>
                                </Form.Item>
                                <Form.Item name="phone" label="Telefon">
                                    <Input></Input>
                                </Form.Item>
                                <Form.Item name="company" label="Çalıştığı Kurum">
                                    <Input></Input>
                                </Form.Item>
                                <Form.Item name="password" label="Etkinlik Şifresi">
                                    <Input.Password></Input.Password>
                                </Form.Item>
                                <Form.Item>
                                    <Checkbox>
                                    Yukarıda verilen bilgilerimin doğru ve güncel olduğunu, toplantı konusu ile ilgili alanda uzman olduğumu/görev yaptığımı, 
                                    <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/terms-of-use/tr-index.html"> Kullanım Şartlarını </a>,
                                    <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/kisisel-veri-politikasi/tr-index.html"> Kişisel Veri İşleme Politikasını </a>
                                    ve <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/aydinlatma-metni/tr-index.html"> Aydınlatma Metnini </a>
                                    okuyup anladığımı ve kendi irademle rıza gösterdiğimi beyan ederim.
                                    </Checkbox>
                                </Form.Item>
                                <Form.Item>
                                    <Checkbox>
                                        Alınan kişisel bilgilerimin etkinliği düzenleyen ilaç firması ile, Sağlık Bakanlığı'na bildirimde bulunması amacıyla, paylaşılmasına izin veriyorum.
                                    </Checkbox>
                                </Form.Item>
                                <Form.Item>
                                    <Button type="success" block>
                                        Kayıt Ol
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
        </>
    )
}

export default LoginPage;