import React from 'react';
import {Route, Switch} from 'react-router-dom';
import AdminLayoutWrapper from '../../layouts/admin';
import AdminIndex from './index';
import AddCompany from './companies/new';
import EditCompany from './companies/edit';
import IndexCompany from './companies/index';
import IndexEvent from './events/index';
import AddEvent from './events/new';
import IndexBranch from './branches/index';
import AddBranch from './branches/add';
import EditBranch from './branches/edit';

import NotFound from '../notFound';


function AdminRoot() {
    return (
        <AdminLayoutWrapper>
            <Switch>
                <Route path="/admin" exact component={AdminIndex} />
                <Route path="/admin/company" exact component={IndexCompany} />
                <Route path="/admin/company/add" component={AddCompany} />
                <Route path="/admin/company/edit" component={EditCompany} />
                <Route path="/admin/event" exact component={IndexEvent} />
                <Route path="/admin/event/add" component={AddEvent} />
                <Route path="/admin/branch" exact component={IndexBranch} />
                <Route path="/admin/branch/add" component={AddBranch} />
                <Route path="/admin/branch/edit" component={EditBranch} />
                <Route path="*" component={NotFound} />
            </Switch>
        </AdminLayoutWrapper>
    )
}
export default AdminRoot;
