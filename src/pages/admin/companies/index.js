import React, { Component } from 'react'
import { Card, Table, Space, Button } from 'antd';
import { get } from '../../../libs/connection';
import moment from 'moment';

export default class index extends Component {
    state = {
        companiesList: ''
    };

    async componentDidMount(){
        console.log('Burası her zaman yüklenecek');
        const companies = await get('/company');
        if(companies) {
            this.setState((state) => {
                return { companiesList : companies} 
            });
        }
        console.log(companies);
    }

    setAttributesTable = (id) => {
        return (
            <Space size="middle">
                <Button size="small" type="dashed" onClick={() => this.props.history.push({pathname: '/admin/company/edit', state: {id}})}>Düzenle</Button>
                <Button size="small" type="dashed" onClick={() => console.log(id)}>Sil</Button>
            </Space>
        )
    }

    render() {
        return (
            <Card size="small" title="Firmalar" style={{ width: '100%', margin: 10 }}>
                <Table columns={this.columns} dataSource={this.state.companiesList} rowKey='_id' bordered size="medium"/>
            </Card>
        )
    }

    columns = [
            {title: 'Sıra', key: 'id', dataIndex: 'id', render: (text, record, index) => index + 1},
            {title: 'Firma Adı', key: 'name', dataIndex: 'name'},
            {
                title: 'Eklenme Tarihi',
                key: 'createdAt',
                dataIndex: 'createdAt',
                render: (key, record) => {
                    return (
                        moment(record.createdAt).format('DD.MM.YYYY')
                    )
                }
            },
            {
                title: 'Aktif/Pasif',
                key: 'status',
                dataIndex: 'status',
                render: (key, record) => {
                    return (
                        <>
                        {record.status? "Aktif": "Pasif"} 
                        </>
                    )
                }
            },
            {
                title: 'Özellikler',
                key: 'attribute',
                width: '100px',
                dataIndex: 'attribute',
                render: (key, record) => {
                    if (record) {
                        return this.setAttributesTable(record._id);
                        // this.props.router.push({pathname: '/admin', state: {record}})
                    }
                }
            }
        ]

}
