import React, { Component } from 'react'
import { Card, Form, Input, Button, Upload, message, Checkbox, Modal } from 'antd';
import { get, post } from '../../../libs/connection';

const showModal = (message) => Modal.success({
    title: 'Bilgi Mesajı',
    content: message,
});

const validateMessages = {
    required: "Bu alan gereklidir!",
};

export class EditCompany extends Component {
    formRef = React.createRef();
    state = {
      id: this.props.location.state.id,
      loading: false,
      faviconFile: null,
      logoFile: null,
      favicon: null,
      logo: null,
    }

    async componentDidMount(){
      const company = await get(`/company/${this.state.id}`);
      let faviconFile, logoFile;
      if(company.favicon) {
        faviconFile = [{
          fieldname: 'favicon',
          uid: '-1',
          name: company.favicon,
          url: `http://localhost:3000/uploads/favicon/${company.favicon}`
        }];
      } else {
        faviconFile = null;
      }

      if(company.logo) {
        logoFile = [{
          fieldname: 'logo',
          uid: '-1',
          name: company.logo,
          url: `http://localhost:3000/uploads/logo/${company.logo}`
        }];
      } else {
        logoFile = null;
      }

      this.setState((state) => ({
          value: company,
          favicon : company.favicon,
          logo : company.logo,
          faviconFile,
          logoFile,

      }));
      this.formRef.current.setFieldsValue(company);
  }

    onFinish = async values => {
      // console.log(values);
      const vv = {...values, logo: this.state.logo, favicon: this.state.favicon}
      console.log(vv);
      const result = await post(`/company/${this.state.id}`, vv);
      console.log(result);
      if(result.success) {
          showModal(result.message);
          this.props.history.push('/admin/company/');
      } else {
        console.log(result);
      }
    };

    onRemoveFile = async (value) => {
      console.log(value);
      this.setState(() => {
        return { 
          [value.fieldname]: '',
          [value.fieldname+'File']: '',
        }
      })
    }

    validateUploadFile = (file) => {
      console.log(file);
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        message.error('Sadece JPF/PNG uzantılı resim formatlarını yükleyebilirsiniz!');
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error('Yüklemeye çalıştığınız dosya boyutu çok yüksek, dosya boyutunu küçültüp tekrar deneyiniz!');
      }
      return isJpgOrPng && isLt2M;
    }

    beforeUploadFavicon = (file) => {
      if(this.validateUploadFile(file) === false) return false;
      console.log('beforeUploadFavicon');
    }

    beforeUploadLogo = (file) => {
      if(this.validateUploadFile(file) === false) return false;
    }

    onChange(info) {
      console.log(info);
      const _response = info.file;
      if(info.file.status === 'removed') {
        post('/company/deletePicture', {filename: _response.name, fieldname: _response.fieldname});
      }

      if (info.file.status !== 'uploading') {
        // console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        console.log('DONE OLDU');
        const _key = info.file.response.fieldname;
        const _value = info.file.response.filename;
        this.setState((state) => {
          return {[_key]: _value}
        });
        // message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        console.log('ERROR OLDU');
        // message.error(`${info.file.name} file upload failed.`);
      }
    }
  
    uploadButton = (
      <div style={{ marginTop: 2 }}>+</div>
    );

  render() {
      return (
          <Card size="small" title="Firma Ekle" style={{ width: '100%', margin: 10 }}>
              <Form
                  validateMessages={validateMessages}
                  layout="vertical"
                  size="large"
                  onFinish={this.onFinish}
                  name="company_edit"
                  ref={this.formRef}
              >
                  <Form.Item
                    name="name"
                    label="Firma Adı"
                    rules={[{ required: true }]}
                  >
                    <Input></Input>
                  </Form.Item>
                  <Form.Item
                  label="Favicon"
                  valuePropName="favicon"
                  >
                    <Upload
                        listType="picture-card"
                        showUploadList={true}
                        beforeUpload={this.beforeUploadFavicon}
                        onRemove={this.onRemoveFile}
                        onChange={this.onChange.bind(this)}
                        fileList={this.state.faviconFile}
                        action="http://localhost:3001/company/uploadPicture"
                        name="favicon"
                    >
                        { this.state.favicon ? null : this.uploadButton }
                    </Upload>
                  </Form.Item>
                  <Form.Item
                    label="Logo"
                    valuePropName="logo"
                  >
                  <Upload
                      listType="picture-card"
                      showUploadList={true}
                      name="logo"
                      beforeUpload={this.beforeUploadLogo}
                      onRemove={this.onRemoveFile}
                      onChange={this.onChange.bind(this)}
                      fileList={this.state.logoFile}
                      action="http://localhost:3001/company/uploadPicture"
                  >
                      { this.state.logo ? null : this.uploadButton }
                  </Upload>
                  </Form.Item>
                  <Form.Item
                    name="status"
                    valuePropName="checked"
                  >
                  <Checkbox value={0}> Aktif </Checkbox>
                  </Form.Item>
                  <Button size="middle" type="primary" htmlType="submit">Güncelle</Button>
                  <Button size="middle" onClick={() => this.props.history.push('/admin/company/')}>İptal</Button>
              </Form>
          </Card>
      )
  }
}

export default EditCompany
