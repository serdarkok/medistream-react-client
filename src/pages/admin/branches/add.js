import React, { Component } from 'react'
import {Form, Input, Card, Checkbox, Button, Modal} from 'antd';
import {post} from '../../../libs/connection';

const validateMessages = {
    required: "Bu alan gereklidir!",
};

const showModal = (message) => Modal.success({
    title: 'Bilgi Mesajı',
    content: message,
});

class AddBranch extends Component {
    onFinish = async values => {
        const result = await post('/branch', values);
        console.log(result);
        if(result.success) {
            showModal(result.message);
            this.props.history.push('/admin/branch/');
        } else {
          showModal(result.error.data);
          console.log(result.error.data);
        }
      };

    render() {
        return (
            <Card size="small" title="Branş Ekle" style={{ width: '100%', margin: 10 }}>
              <Form
                  validateMessages={validateMessages}
                  layout="vertical"
                  size="large"
                  onFinish={this.onFinish.bind(this)}
                  name="events"
              >
                  <Form.Item
                    label="Branş Adı"
                    name="name"
                    rules={[{ required: true }]}
                  >
                    <Input></Input>
                  </Form.Item>
                  <Form.Item
                    name="status"
                    valuePropName="checked"
                  >
                    <Checkbox>Aktif mi?</Checkbox>
                  </Form.Item>
                <Button size="middle" type="primary" htmlType="submit">Kaydet</Button>
                <Button size="middle" onClick={() => this.props.history.push('/admin/branch/')}>İptal</Button>
              </Form>
            </Card>
        )
    }
}

export default AddBranch;
