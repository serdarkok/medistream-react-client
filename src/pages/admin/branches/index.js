import React, { Component } from 'react';
import { Card, Table, Space, Button, Popconfirm } from 'antd';
import { get, remove } from '../../../libs/connection';

class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            branchList : null,
        }
    }

    onFilterAfterDelete(id) {
        const branchList = this.state.branchList;
        return branchList.filter(branch => branch._id !== id);
    }

    onDelete = (id) => {
        remove('/branch', id);
        this.setState({
            branchList: this.onFilterAfterDelete(id),
        });
    }

    setAttributesTable = (id) => {
        return (
            <Space size="middle">
                <Button size="small" type="dashed" onClick={() => this.props.history.push({pathname: '/admin/branch/edit', state: {id}})}>Düzenle</Button>
                <Popconfirm
                    title="Kalıcı olarak silinecektir?"
                    onConfirm={() => this.onDelete(id)}
                    okText="Evet"
                    cancelText="Hayır"
                >
                    <Button size="small" type="dashed">Sil</Button>
                </Popconfirm>
            </Space>
        )
    }

    async componentDidMount() {
        const _result = await get('/branch');
        console.log(_result);
        this.setState({
            branchList: _result,
        });
    }

    render() {
        return (
            <Card size="small" title="Branşlar" style={{ width: '100%', margin: 10 }}>
                <Table columns={this.columns} dataSource={this.state.branchList} bordered rowKey="_id" size="middle" />
            </Card>
        )
    }

    columns = [
        {title: 'Sıra', key: 'id', dataIndex: 'id', render: (text, record, index) => index + 1},
        {title: 'Branş Adı', key: 'name', dataIndex: 'name'},
        {
            title: 'Aktif/Pasif',
            key: 'status',
            dataIndex: 'status',
            render: (key, record) => {
                return (
                    <>
                    {record.status? "Aktif": "Pasif"} 
                    </>
                )
            }
        },
        {
            title: 'Özellikler',
            key: 'attribute',
            width: '100px',
            dataIndex: 'attribute',
            render: (key, record) => {
                if (record) {
                    return this.setAttributesTable(record._id);
                    // this.props.router.push({pathname: '/admin', state: {record}})
                }
            }
        }
    ]
}

export default index;