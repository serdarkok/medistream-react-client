import React, { Component } from 'react';
import { Card, Form, Input, Button, Checkbox, Modal } from 'antd';
import { get, post } from '../../../libs/connection';

const showModal = (message) => Modal.success({
    title: 'Bilgi Mesajı',
    content: message,
});

const validateMessages = {
    required: "Bu alan gereklidir!",
};

export default class edit extends Component {
    formRef = React.createRef();

    async componentDidMount(){
        console.log(this.props.location);
        const _result = await get(`/branch/${this.props.location.state.id}`);
        this.formRef.current.setFieldsValue(_result);
    }

    onFinish = async values => {
        console.log(values);
        const result = await post(`/branch/${this.props.location.state.id}`, values);
        console.log(result);
        if(result.success) {
            showModal(result.message);
            this.props.history.push('/admin/branch/');
        } else {
          console.log(result);
        }
      };

    render() {
        return (
            <Card size="small" title="Branş Ekle" style={{ width: '100%', margin: 10 }}>
              <Form
                  validateMessages={validateMessages}
                  layout="vertical"
                  size="large"
                  onFinish={this.onFinish.bind(this)}
                  name="events"
                  ref={this.formRef}
              >
                  <Form.Item
                    label="Branş Adı"
                    name="name"
                    rules={[{ required: true }]}
                  >
                    <Input></Input>
                  </Form.Item>
                  <Form.Item
                    name="status"
                    valuePropName="checked"
                  >
                    <Checkbox>Aktif mi?</Checkbox>
                  </Form.Item>
                <Button size="middle" type="primary" htmlType="submit">Kaydet</Button>
                <Button size="middle" onClick={() => this.props.history.push('/admin/branch/')}>İptal</Button>
              </Form>
            </Card>
        )
    }
}
