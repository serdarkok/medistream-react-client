import React, { Component } from 'react';
import { Card, Form, Input, Select, DatePicker, Button, Checkbox, Modal, Divider, Row } from 'antd';
import ReactHtmlParser from 'react-html-parser';
import { get } from '../../../libs/connection';
import MakeForm from '../../../components/makeForm';
import UploadBanner from '../../../components/uploadBanner';
import fields from '../../../libs/presetFormItems';

const validateMessages = {
    required: "Bu alan gereklidir!",
};
  
/* const showModal = (message) => Modal.success({
  title: 'Bilgi Mesajı',
  content: message,
}); */

export default class AddEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      company: [],
      branch: [],
      fields: fields,
      uploaded: [],
      uploadButtons : [
        {name: 'desktop-left', title: 'Masaüstü Sol'},
        {name: 'desktop-right', title: 'Masaüstü Sağ'},
        {name: 'mobil-top', title: 'Mobil Üst'},
        {name: 'mobil-bottom', title: 'Mobil Alt'},
      ],
      selectedBranch: [],
      confirm1: `Yukarıda verilen bilgilerimin doğru ve güncel olduğunu, toplantı konusu ile ilgili alanda uzman olduğumu/görev yaptığımı, <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/terms-of-use/tr-index.html"> Kullanım Şartlarını </a>, <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/kisisel-veri-politikasi/tr-index.html"> Kişisel Veri İşleme Politikasını </a> ve <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/aydinlatma-metni/tr-index.html"> Aydınlatma Metnini </a> okuyup anladığımı ve kendi irademle rıza gösterdiğimi beyan ederim.`,
      confirm2: `Alınan kişisel bilgilerimin etkinliği düzenleyen ilaç firması ile, Sağlık Bakanlığı'na bildirimde bulunması amacıyla, paylaşılmasına izin veriyorum.`,
      showFormBuilderModal: false,
      showPasswordInput: false,
     }
  }

  onFinish(values) {
    const data = {...values, fields: this.state.fields}
    console.log(data);
  };

  showURL(value) {
    this.setState({
      url : value,
    })
  };

  changePasswordCheckbox(e) {
    this.setState({
      showPasswordInput : e.target.checked
    });
    this.setState((state) => {
      state.fields.map((el) => el.key === e.target.name ? el.status = e.target.checked : '');
    });
  };

  async componentDidMount() {
    const company = await get('/company/getall');
    if(company) this.setState({company});
    const branch = await get('/branch');
    if(branch) this.setState({branch});
  }

  handleBranch = (values) => {
    const branchList = this.getBranchOptions();
    const selectedBranch = values.map((f) => {
      return branchList.find((e) => e.value === f);
    });

    this.setState((state) => {
      state.fields.map((el) => el.key === 'branch' ? el.data = selectedBranch : '');
    });
  }

  handleUploadedFileFromChild = (key, name) => {
    this.setState((state) => {
      state.uploaded = [...state.uploaded, {key, name}]
    })
  }

  getBranchOptions = () => this.state.branch.map(val => { return { label: val.name, value: val._id } });

  addPassword = (e) => {
    e.persist();
    this.setState((state) => {
      state.fields.map((el) => el.key === e.target.name ? el.data = e.target.value : '' );
    });
  }

  showPasswordInput = (
    <Form.Item
    label="Şifre"
    name="password"
    rules={[{ required: true }]}
  >
    <Input
      name="password"
      onChange={(e) => this.addPassword(e)}
    >
    </Input>
  </Form.Item>
  );

  changeValue = (e) => {
    e.persist();
    this.setState((state) => {
      state.fields.map((el) => el.key === e.target.name ? el.data = ReactHtmlParser(e.target.innerHTML) : '' );
    });
/*       state.fields[e.target.name] = {...state.fields[e.target.name], data: ReactHtmlParser(e.target.innerHTML)}
    }, (d) => console.log(this.state.fields)); */
  }

  setFieldsfromChildComp = (fields) => {
    fields.map((el) => el.key === 'password' ? this.setState({showPasswordInput : el.status}) : '')
    this.setState({
      fields
    });
  }

  render() {
      return (
          <Card size="small" title="Etkinlik Ekle" style={{ width: '100%', margin: 10 }}>
              <Form
                  validateMessages={validateMessages}
                  layout="vertical"
                  size="large"
                  onFinish={this.onFinish.bind(this)}
                  name="events"
                  initialValues={{
                    confirm1: this.state.confirm1,
                    confirm2: this.state.confirm2,
                  }}
              >
                  <div className="slugURL">https://medistream.com/{this.state.url}</div>
                  <Form.Item
                      label="Etkinlik Linki"
                      name="url"
                      rules={[
                        { required: true },
                        {
                          pattern: new RegExp(/^[A-Za-z0-9-]+$/i),
                          message: "Sadece türkçe olmayan karakterler, rakamlar ve - işareti kullanabilirsiniz!"
                        }
                      ]}
                  >
                    <Input
                      maxLength={20}
                      onChange={(e) => this.showURL(e.target.value)}
                    >
                    </Input>
                  </Form.Item>
                  <Form.Item
                    label="Etkinlik Adı"
                    name="title"
                    // rules={[{ required: true }]}
                  >
                    <Input>
                    </Input>
                  </Form.Item>
                  <Form.Item
                    label="Firma"
                    // rules={[{ required: true }]}
                    name="company"
                  >
                    <Select
                      style={{ width: 220 }}
                    >
                      {this.state.company.map(item => (
                        <Select.Option key={item._id}>{item.name}</Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Branşlar"
                    // rules={[{ required: true }]}
                    name="branch"
                  >
                    <Select
                      mode="multiple"
                      style={{ width: '220px' }}
                      showArrow={true}
                      maxTagCount={0}
                      placeholder="Lütfen Seçiniz..."
                      onChange={this.handleBranch}
                      options={this.getBranchOptions()}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Başlangıç Zamanı"
                    // rules={[{ required: true }]}
                    name="start_date"
                  >
                    <DatePicker 
                    style={{ width: 220 }}
                    showTime={true}
                    minuteStep={10}
                    secondStep={10}
                    format={'DD.MM.YYYY H:mm:ss'}
                    
                  />
                  </Form.Item>
                  <Form.Item
                    label="Bitiş Zamanı"
                    // rules={[{ required: true }]}
                    name="end_date"
                  >
                    <DatePicker 
                    style={{ width: 220 }}
                    showTime={true}
                    minuteStep={10}
                    secondStep={10}
                    format={'DD.MM.YYYY H:mm:ss'}
                    
                  />
                  </Form.Item>
                  <Form.Item
                    label="Kayıt Başlama Zamanı"
                    // rules={[{ required: true }]}
                    name="registration_start_date"
                  >
                    <DatePicker 
                    style={{ width: 220 }}
                    showTime={true}
                    minuteStep={10}
                    secondStep={10}
                    format={'DD.MM.YYYY H:mm:ss'}
                    
                  />
                  </Form.Item>
                  <Form.Item
                    name="question"
                    valuePropName="checked"
                  >
                    <Checkbox>Katılımcılar soru sorabilecek mi?</Checkbox>
                  </Form.Item>
                  <Form.Item
                    name="passwordCheckbox"
                  >
                    <Checkbox
                      name="passwordCheckbox"
                      onChange={(e) => this.changePasswordCheckbox(e)}
                      checked={this.state.showPasswordInput}
                    >
                      Şifreli giriş olacak mı?
                    </Checkbox>
                  </Form.Item>
                  { this.state.showPasswordInput ? this.showPasswordInput : '' }

                  <Form.Item
                  name="speakers"
                  label="Konuşmacılar"
                  >
                    <Input.TextArea
                    name="speakers"
                    rows={4}
                    onBlur={(e) => this.changeValue(e)}
                    />
                  </Form.Item>

                  <Form.Item
                  name="confirm1"
                  label="Aydınlatma Metni (confirm-1)"
                  >
                    <Input.TextArea
                    name="confirm1"
                    rows={4}
                    onBlur={(e) => this.changeValue(e)}
                    />
                  </Form.Item>

                  <Form.Item
                  name="confirm2"
                  label="Onay (confirm-2)"
                  >
                    <Input.TextArea
                      name="confirm2"
                      rows={4}
                      onBlur={(e) => this.changeValue(e)}
                    />
                  </Form.Item>

                  <Divider orientation="left" plain>Banner Yüklemeleri</Divider>

                  <Row gutter={[20, 20]}>
                        {this.state.uploadButtons.map((el, index) => <UploadBanner value={el} key={index} handleUploadedFileFromChild={this.handleUploadedFileFromChild} />)}
                  </Row>

                  <Form.Item
                    name="status"
                    valuePropName="checked"
                  >
                    <Checkbox>Aktif mi?</Checkbox>
                  </Form.Item>
                  
                  <Form.Item>
                  <Button size="medium" onClick={() => this.setState({showFormBuilderModal : true})}>Form Yap</Button>
                  </Form.Item>

                    <Button size="middle" type="primary" htmlType="submit">Kaydet</Button>
                    <Button size="middle" onClick={() => this.props.history.push('/admin/company/')}>İptal</Button>
              </Form>
              <Modal
                title="Form Ekle/Düzenle"
                visible = {this.state.showFormBuilderModal}
                onOk={() => this.setState({ showFormBuilderModal : false })}
                footer={
                  <Button type="primary" onClick={() => this.setState({ showFormBuilderModal : false })}>Kapat</Button>
                }
              >
                <MakeForm putFields={this.state.fields} setFieldsfromChildComp={this.setFieldsfromChildComp} />
              </Modal>
          </Card>
      )
  }
}
