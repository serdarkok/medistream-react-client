import React from 'react';
import { Card } from 'antd';

const AdminIndex = () => {
    return (
        <Card size="small" title="Small size card" extra={<a href="/">More</a>} style={{ width: '100%', margin: 10 }}>
            <div>Admin Panel</div>
        </Card>
    )
}

export default AdminIndex;