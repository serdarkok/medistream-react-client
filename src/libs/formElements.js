import React, { Component } from 'react'
import { Input, Form, Select, Checkbox } from 'antd';
import ReactHtmlParser from 'react-html-parser';
import {sortableContainer, sortableElement, sortableHandle} from 'react-sortable-hoc';
import arrayMove from 'array-move';

const DragHandle = sortableHandle(() => <span className="draggable-icon">&#8645;</span>);

const SortableItem = sortableElement(
    ({value, changeStatus}) => {
        const Bb = components[value.type];
        return (
            <li>
                <Bb name={value.label} label={value.label} data={value.data} />
                <Checkbox className="draggable-checkbox" checked={value.status} onChange={() => changeStatus(value.key)}></Checkbox>
                <DragHandle />
            </li> );
    });

const SortableContainer = sortableContainer(({children}) => {
    return <ul className="draggable-wrapper">{children}</ul>;
});

const FormInput = (props) => {
    return (
        <Form.Item
            label={props.label}
            name={props.name}
            initialValue={props.data}
        >
            <Input style={{width: '80%'}} disabled/>
        </Form.Item>
    )
}

const FormSelect = (props) => {
    return (
        <Form.Item
        label={props.label}
        name={props.name}
        >
        <Select
          mode="multiple"
          showArrow={true}
          style={{width: '80%'}}
          maxTagCount={0}
          placeholder="Lütfen Seçiniz..."
          options={props.data}
        />
      </Form.Item>
    )
}

const FormCheckbox = (props) => {
    return (
        <Form.Item
        name={props.name}
        valuePropName="checked"
        >
            <Checkbox
                style={{width: '80%'}}
                disabled
            >
                { ReactHtmlParser(props.data) }
            </Checkbox>
        </Form.Item>
    )
}

const components = {
    FormInput,
    FormSelect,
    FormCheckbox
}

class FormBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: this.props.fields,
        }
    }

    onSortEnd = ({oldIndex, newIndex}) => {
                    this.setState(({fields}) => ({
                        fields: arrayMove(fields, oldIndex, newIndex),
                    }));
                    this.props.saveFields(this.state.fields);
    };

    render() {
        const {fields} = this.state;
        return (
             <SortableContainer onSortEnd={this.onSortEnd} useDragHandle>
                {fields.map((value, index) => (
                    <SortableItem key={index} index={index} sortIndex={index} value={value} changeStatus={this.props.changeStatus} />
                ))}
            </SortableContainer>
        );
    }
}

export default FormBuilder;