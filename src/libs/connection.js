import axios from 'axios';
const apiurl = process.env.REACT_APP_API_URL;

async function get(endpoint, data) {
  const response = await axios({
    method: 'get',
    params: {
      ...data
    },
    headers: {
      Authorization: `jwt ${localStorage.getItem('access')}`,
    },
    url: `${apiurl}${endpoint}`,
  });
  return response.data;
}

async function remove(endpoint, data) {
  const response = await axios({
    method: 'delete',
    params: {
      data
    },
    headers: {
      Authorization: `jwt ${localStorage.getItem('access')}`,
    },
    url: `${apiurl}${endpoint}`,
  });
  console.log(response);
}

async function postWithFile(endpoint, values, files) {
  let response;
  const fd = new FormData();
  Object.keys(values).map(el => fd.append(el, values[el]));
  //files.forEach((element) => {
  //  fd.append(Object.keys(element), element);
  // });
  try {
    response = await axios({
      data: fd,
      method: 'post',
      headers: {
        Authorization: `jwt ${localStorage.getItem('access')}`,
        'Content-Type': 'multipart/form-data',
      },
      url: `${apiurl}${endpoint}`,
      validateStatus: status => status < 500,
    });
    return {
      success: true,
      message: response.data
    }
  } catch (e) {
    return {
      success: false,
      error: e.response,
    };
  }
}

async function post(endpoint, values) {
    let response;
    // const fd = new FormData();
    // Object.keys(values).map(el => fd.append(el, values[el]));
    try {
      response = await axios({
        data : values,
        method: 'post',
        headers: {
          Accept: 'application/json',
          Authorization: `jwt ${localStorage.getItem('access')}`,
        },
        url: `${apiurl}${endpoint}`,
        validateStatus: status => status < 500,
      });
      return {
        success: true,
        message: response.data
      }
    } catch (e) {
      return {
        success: false,
        error: e.response,
      };
    }
}

export {
  post,
  postWithFile,
  remove,
  get
}