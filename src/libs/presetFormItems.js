const fields = [
    { key: 'title', label: 'Unvan', type: 'FormInput', status: true },
    { key: 'fullname', label: 'İsim Soyisim', type: 'FormInput', status: true },
    { key: 'id', label: 'TC Kimlik No', type: 'FormInput', status: true },
    { key: 'branch', label: 'Branşlar', type: 'FormSelect', status: true, data: null },
    { key: 'email', label: 'E-Posta', type: 'FormInput', status: true },
    { key: 'phone', label: 'Telefon', type: 'FormInput', status: true },
    { key: 'company', label: 'Çalıştığı Kurum', type: 'FormInput', status: true },
    { key: 'password', label: 'Şifre', type: 'FormInput', status: false },
    { key: 'confirm1', label: 'Aydınlatma Metni', type: 'FormCheckbox', status: true, data: `Yukarıda verilen bilgilerimin doğru ve güncel olduğunu, toplantı konusu ile ilgili alanda uzman olduğumu/görev yaptığımı, <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/terms-of-use/tr-index.html"> Kullanım Şartlarını </a>, <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/kisisel-veri-politikasi/tr-index.html"> Kişisel Veri İşleme Politikasını </a> ve <a target="_blank" rel="noopener noreferrer" href="http://www.turkiyeklinikleri.com/aydinlatma-metni/tr-index.html"> Aydınlatma Metnini </a> okuyup anladığımı ve kendi irademle rıza gösterdiğimi beyan ederim.` },
    { key: 'confirm2', label: 'Aydınlatma Metni', type: 'FormCheckbox', status: true, data: `Alınan kişisel bilgilerimin etkinliği düzenleyen ilaç firması ile, Sağlık Bakanlığı'na bildirimde bulunması amacıyla, paylaşılmasına izin veriyorum.` },
  ];

  export default fields;