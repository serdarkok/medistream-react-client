import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

const NavBar = () => {
    return (
        <Menu mode="horizontal">
            <Menu.Item> Ana Sayfa </Menu.Item>
            <Menu.SubMenu title="Branş">
                <Menu.Item>
                    Ekle
                    <Link to="/admin/branch/add" />
                </Menu.Item>
                <Menu.Item>
                    Listele
                    <Link to="/admin/branch/" />
                </Menu.Item>
            </Menu.SubMenu>
            <Menu.SubMenu title="Firma">
                <Menu.Item>
                    Ekle
                    <Link to="/admin/company/add" />
                </Menu.Item>
                <Menu.Item>
                    Listele
                    <Link to="/admin/company/" />
                </Menu.Item>
            </Menu.SubMenu>
            <Menu.SubMenu title="Event">
                <Menu.Item>
                    Ekle
                    <Link to="/admin/event/add" />
                </Menu.Item>
                <Menu.Item>
                    Listele
                    <Link to="/admin/event/" />
                </Menu.Item>
            </Menu.SubMenu>
            <Menu.Item> İstatistikler </Menu.Item>
        </Menu>
    )
}

export default NavBar;