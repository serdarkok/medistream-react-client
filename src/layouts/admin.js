import React from 'react';
import { Layout, Row, Col } from 'antd';
import NavBar from './navBar';
import 'antd/dist/antd.css';

const AdminLayoutWrapper = (props) => {
    return (
        <Layout className={props.className}>
            <Row>
                <Col span={24}>
                    <NavBar />
                </Col>
            </Row>
            <Row>
                <Col span={14} offset={5}>
                    { props.children }
                </Col>
            </Row>
        </Layout>
    )
}

export default AdminLayoutWrapper;