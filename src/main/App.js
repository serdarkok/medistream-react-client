import React from 'react';
import Counter from '../components/count';
import './App.css';
import LayoutWrapper from '../layouts/admin';

function App() {
  return (
    <div className="App">
      <LayoutWrapper className="layout-wrapper">
        <Counter />
      </LayoutWrapper>
    </div>
  );
}

export default App;
