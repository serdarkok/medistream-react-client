import React, { Component } from 'react';
import { Form } from 'antd';
import FormBuilder from '../libs/formElements';

// const titles = ['Prof.', 'Doç.', 'Öğr. Gör.', 'Uzm.', 'Aile Hekimi', 'Asistan', 'Eczacı', 'Firma Çalışanı'];
// const branches = ['Kardiyoloji', 'Göğüs Hastalıkları', 'Gastroenteroloji', 'Aile Hekimi'];
export default class MakeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
        fields: this.props.putFields,
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      const fields = this.props.putFields;
      this.setState({
        fields
      });
    }
  }

    changeStatus = (data) => {
      const {fields} = this.state;
      fields.forEach((el) => {
        if(el.key === data) el.status = !el.status
      });
      this.setState({
        fields,
      });
      this.props.setFieldsfromChildComp(this.state.fields);
    }

    saveFields = (fields) => {
      this.setState({
        fields
      });
      this.props.setFieldsfromChildComp(this.state.fields);
    }

    render() {
        return (
            <Form size="large">
              <FormBuilder fields={this.state.fields} changeStatus={this.changeStatus} saveFields={this.saveFields} />
            </Form>
          )
    }
}
