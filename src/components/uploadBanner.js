import React, { Component } from 'react';
import { Col, Card, Form, Upload, Button, Input, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { post } from '../libs/connection';

export default class uploadBanner extends Component {
    state = {
        uploaded: [],
        fileList: [],
    }
    handleUploadFile = ({file, fileList}) => {
        let newfileList = [...fileList];
        newfileList = newfileList.slice(-1);
        this.setState({fileList: newfileList});
        if (file.status === 'done') {
            message.success(`Dosya başarılı bir şekilde yüklendi :)`);
            this.props.handleUploadedFileFromChild(this.props.value.name, file.response);
            this.setState({
                uploaded: {key: this.props.value.name, name: file.response}
            });
            console.log(file.response);
        }

        if (file.status === 'error') {
            console.log(file.response);
            message.error(`Dosya yüklemede bir hata oluştu!`);
        }
    }

    beforeUpload = (file) => {
        console.log(file);
        if(this.state.uploaded) {
            post('/event/removeuploadedimage', this.state.uploaded);
        }
    }

    onRemoveUpload = (file) => {
        console.log(file);
    }

    render() {
        const props = {
            name: 'file',
            action: 'http://localhost:3001/event/uploadImage',
            accept: ".jpg,.jpeg,.gif,.png",
        }
        return (
            <Col className="gutter-row" md={12} xl={12} xs={24}>
                <Card>
                    <div className="upload-card-title">{this.props.value.title}</div>
                    <Form.Item
                        name={this.props.value.name}
                        className="upload-input"
                    >
                        <Upload
                            {...props}
                            fileList={this.state.fileList}
                            onChange={this.handleUploadFile}
                            beforeUpload={this.beforeUpload}
                            onRemove={this.onRemoveUpload}
                        >
                        <Button icon={<UploadOutlined />} size="mini">Yükle</Button>
                        </Upload>
                        <Input addonBefore="Link:" size="small" style={{marginTop: "5px"}}></Input>
                    </Form.Item>
                </Card>
            </Col>
        )
    }
}
